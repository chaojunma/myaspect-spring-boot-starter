package com.wink.autoconfiguration.myaspect;

import java.lang.annotation.*;

/**
 * 重复提交
 * 自定义注解防止表单重复提交
 *
 * @author xw
 * @date 2020/11/16 17:12:58
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
    /**
     * 指定时间内不可重复提交,单位：秒
     */
    long expire() default 3;

    /*业务key 支持spel表达式 纯粹花着写而已 可以不用的*/
    String keys() default "";

    String message() default "请勿重复提交！";

}
