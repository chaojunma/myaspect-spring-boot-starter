package com.wink.autoconfiguration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 配置文件对应类
 *
 * @author xw
 * @date 2020-11-24 10:00
 */
@Configuration
@ConfigurationProperties(value = "repeatSubmit")
public class MyAspectProperties {

    private boolean repeatSubmitEnable;

    public boolean isRepeatSubmitEnable() {
        return repeatSubmitEnable;
    }


    public void setRepeatSubmitEnable(boolean repeatSubmitEnable) {
        this.repeatSubmitEnable = repeatSubmitEnable;
    }
}
